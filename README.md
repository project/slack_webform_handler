# Slack Webform Handler

## Contents of this file

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers

 ## Introduction


## Requirements 
 
 This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


## Maintainers

Current maintainers:
- Mohammad Abdul-Qader - [m.abdulqader](https://www.drupal.org/u/mabdulqader)
