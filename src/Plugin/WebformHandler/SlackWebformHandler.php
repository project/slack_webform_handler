<?php

namespace Drupal\slack_webform_handler\Plugin\WebformHandler;

use Drupal\Core\Utility\Error;
use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "slack_handler",
 *   label = @Translation("Slack message handler"),
 *   category = @Translation("External"),
 *   description = @Translation("Sends a message to Slack on submission."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class SlackWebformHandler extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'webhook_url' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['webhook_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Slack Webhook URL'),
      '#description' => $this->t('Enter the URL of the Slack webhook that you want to post to.'),
      '#default_value' => $this->configuration['webhook_url'],
    ];
    $form['image'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image URL'),
      '#description' => $this->t('Add image inside the notification.'),
      '#default_value' => $this->configuration['image'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['webhook_url'] = $form_state->getValue('webhook_url');
    $this->configuration['image'] = $form_state->getValue('image');
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {

    // Retreiver handler configration.
    $webhook_url = $this->configuration['webhook_url'];
    $message_image = $this->configuration['image'];

    // Retrieve weform and submissions details.
    $webform_name = $webform_submission->getWebform()->label();
    $webform_id = $webform_submission->getWebform()->id();
    $data = $webform_submission->getData();
    $submission_date = $webform_submission->getCreatedTime();
    $submission_date_formatted = DateTimePlus::createFromTimestamp($submission_date)->format('Y-m-d H:i:s');
    $submission_id = $webform_submission->id();

    // Generate the URL for the webform submission result page.
    $submission_url = Url::fromRoute('entity.webform_submission.canonical')
      ->setRouteParameter('webform', $webform_id)
      ->setRouteParameter('webform_submission', $submission_id)
      ->setAbsolute()
      ->toString();

    $message_elements = [];
    $message_elements[] = ["type" => "divider"];
    $message_elements[] = [
      "type" => "header",
      "text" => [
        "type" => "plain_text",
        "text" => "You have a new submission on webform $webform_name",
      ],
    ];
    $message_elements[] = ["type" => "divider"];

    if (isset($message_image)) {
      $message_elements[] = [
        "type" => "image",
        "title" => [
          "type" => "plain_text",
          "text" => "You have a new submission",
          "emoji" => TRUE,
        ],
        "image_url" => "$message_image",
        "alt_text" => "You have a new submission on webform $webform_name",
      ];
    }

    $message_elements[] = [
      "type" => "section",
      "text" => [
        "type" => "mrkdwn",
        "text" => "Submitted on: $submission_date_formatted",
      ],
    ];
    $message_elements[] = ["type" => "divider"];

    $fields = [];
    foreach ($data as $field) {
      $fields[] = [
        "type" => "mrkdwn",
        "text" => $field,
      ];
    }

    $message_elements[] = [
      "type" => "section",
      "fields" => $fields,
    ];
    $message_elements[] = ["type" => "divider"];
    $message_elements[] = [
      "type" => "section",
      "text" => [
        "type" => "mrkdwn",
        "text" => "Check the complete submission.",
      ],
      "accessory" => [
        "type" => "button",
        "text" => [
          "type" => "plain_text",
          "text" => "Check Now",
          "emoji" => TRUE,
        ],
        "value" => "submission_link",
        "url" => "$submission_url",
        "action_id" => "button-action",
      ],
    ];
    $message['blocks'] = $message_elements;

    $client = \Drupal::httpClient();

    try {
      $client->post(
        $webhook_url, [
          'headers' => ['Content-Type' => 'application/json'],
          'body' => json_encode($message),
        ]
      );
    }
    catch (RequestException $e) {
      DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.0.0', fn() => Error::logException(\Drupal::logger('slack_webform_handler'), $e), fn() => watchdog_exception('slack_webform_handler', $e));
    }
  }

}
